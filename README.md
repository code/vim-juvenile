Juvenile
========

Dark Vim colorscheme.  Show comments in grey if colors available, in defiance
of Commander Pike, but nothing else, in deference to Commander Pike.  Works
best with 256 colors or a GUI.

<https://groups.google.com/forum/#!msg/golang-nuts/hJHCAaiL0so/kG3BHV6QFfIJ>

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
